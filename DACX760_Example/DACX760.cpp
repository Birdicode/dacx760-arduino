#include "DACX760.h"
#include "definitions.h"
#include <SPI.h>

#define SPI_SPEED       20000000    // 20MHz
#define SPI_ORDER       MSBFIRST    // Most significant bit first
#define SPI_MODE        SPI_MODE0   // SPI Mode



/**
 * As explained in the Serial Peripheral Interface (SPI) section of this document, the DACx760 digital interface
 * constantly clocks in data regardless of the status of the LATCH pin, and data are unconditionally latched on the
 * rising edge of the LATCH pin. A rising edge on the LATCH pin is the only way the device takes action on clocked
 * data.
*/




DACX760::DACX760(int latchPin, modes mode)
{
   // Set the latch pin
   _latchPin   = latchPin;
   _mode       = mode;
}

void DACX760::setup()
{
   // Start the SPI, deselect the chip
   SPI.begin();

   pinMode(_latchPin, OUTPUT);
   digitalWrite(_latchPin, HIGH);
}


void DACX760::enableSingleOutput(outputType outType, outputLimit outLimit, uint16_t value, bool boost)
{
   // Check if we haven't disabled it
      writeControlRegister(outLimit, 0, 0, 0, 0, 1, 0, boost, 0);

      // Write the value
      writeData(value);
}

void DACX760::enableDualOutput(outputLimit outLimitVoltage, outputLimit outLimitCurrent, uint16_t valueVoltage, uint16_t valueCurrent, bool boost)
{
   // Check if they are disabled
   if(outLimitVoltage == DisableV)
      outLimitVoltage = 0b111;

   if(outLimitCurrent == DisableI)
      outLimitVoltage = 0b00;

   // Not connected
   writeConfigRegister(0, 0, 0, 0, 0, 1, outLimitCurrent>>1);
   writeControlRegister(outLimitVoltage&0b111, 0, 0, 0, 0, 1, 0, boost, 0);

   // Write the data
   writeData(valueVoltage);
   writeData(valueCurrent);
}

uint8_t DACX760::readValue(uint16_t registerAddr)
{
   // 0x02 -> Register Read
   SPI.beginTransaction(SPISettings(SPI_SPEED, SPI_ORDER, SPI_MODE));
   digitalWrite(_latchPin, LOW);
 
   // Let's talk
   uint8_t data[3] = {
      0x02,
      registerAddr >> 8,
      registerAddr & 0xFF
   };

   // Transfer the data
   SPI.transfer(&data, 3);
   
   // Read some data
   SPI.transfer(0x00);                          // First 8 bits are empty
   uint8_t dataRead = SPI.transfer(0x00);           // Get the actual data

   // Disable communications
   digitalWrite(_latchPin, HIGH);              // Set pin HIGH
   SPI.endTransaction();

   // Return the data
   return dataRead;
}

void DACX760::writeData(uint16_t dataIn)
{
   // 0x01 -> Write DAC Data register
   SPI.beginTransaction(SPISettings(SPI_SPEED, SPI_ORDER, SPI_MODE));
   digitalWrite(_latchPin, LOW);
 
   #ifdef SerialEN
      // Print some data to the serial connection
      Serial.print("Write data (0x01): ");
      Serial.println(dataIn, BIN);
   #endif

   // Check which mode we are in and shift the bits
   if(_mode == M12bit)
      dataIn = dataIn<<4;

   // Let's talk
   uint8_t data[3] = {
      0x01,
      dataIn >> 8,
      dataIn & 0xFF
   };

   // Transfer the data
   SPI.transfer(&data, 3);

   // Disable communications
   digitalWrite(_latchPin, HIGH);              // Set pin HIGH
   SPI.endTransaction();

   // Return the data
   return data;
}


void DACX760::writeControlRegister(uint8_t range, bool DCEN, bool SREN, uint8_t SRStep, uint8_t SRCLK, bool OUTEN, bool REXT, bool OVR, bool CLRSEL)
{
   // 0x55 -> Write control register
   uint16_t signal = (range & 0b111) | (DCEN << 3) | (SREN << 4) | ((SRStep & 0b111) << 5) | ((SRCLK & 0b1111) << 8) | (OUTEN << 12) | (REXT << 13) | (OVR << 14) | (CLRSEL << 15);

   #ifdef SerialEN
      // Print some data to the serial connection
      Serial.print("Write Control Register (0x55): ");
      Serial.println(signal, BIN);
   #endif

   // Start talking
   SPI.beginTransaction(SPISettings(SPI_SPEED, SPI_ORDER, SPI_MODE));
   digitalWrite(_latchPin, LOW);
 
   // Let's talk
   uint8_t data[3] = {
      0x55,
      signal >> 8,
      signal & 0xFF
   };

   // Transfer the data
   SPI.transfer(&data, 3);

   // Disable communications
   digitalWrite(_latchPin, HIGH);              // Set pin HIGH
   SPI.endTransaction();
}

void DACX760::writeResetRegister()
{
   #ifdef SerialEN
      // Print some data to the serial connection
      Serial.print("Write reset register (0x65)");
   #endif

   // 0x56 -> Write reset register
   // Start talking
   SPI.beginTransaction(SPISettings(SPI_SPEED, SPI_ORDER, SPI_MODE));
   digitalWrite(_latchPin, LOW);
 
   // Let's talk
   uint8_t data[2] = {
      0x56,
      1
   };

   // Transfer the data
   SPI.transfer(&data, 2);

   // Disable communications
   digitalWrite(_latchPin, HIGH);              // Set pin HIGH
   SPI.endTransaction();
}

void DACX760::writeConfigRegister(uint8_t WDPD, bool WDEN, bool HARTEN, bool CALEN, bool APD, bool DUAL_OUTEN, uint8_t range)
{
   // 0x57 Write configuration register
   uint16_t signal = (WDPD & 0b11) | (WDEN << 2) | (HARTEN << 4) | (CALEN << 5) | (APD << 7) | (DUAL_OUTEN << 8) | ((range & 0b11) << 9);

   #ifdef SerialEN
      // Print some data to the serial connection
      Serial.print("Write Config Register (0x57): ");
      Serial.println(signal, BIN);
   #endif

   // Start talking
   SPI.beginTransaction(SPISettings(SPI_SPEED, SPI_ORDER, SPI_MODE));
   digitalWrite(_latchPin, LOW);
 
   // Let's talk
   uint8_t data[3] = {
      0x57,
      signal >> 8,
      signal & 0xFF
   };

   // Transfer the data
   SPI.transfer(&data, 3);

   // Disable communications
   digitalWrite(_latchPin, HIGH);              // Set pin HIGH
   SPI.endTransaction();
}


void DACX760::writeDACGainCalibrationRegister(uint16_t dataIn)
{
   #ifdef SerialEN
      // Print some data to the serial connection
      Serial.print("Write DAC Gain Calibration Register (0x58): ");
      Serial.println(dataIn, BIN);
   #endif

   // 0x58 -> Write DAC gain calibration register
   SPI.beginTransaction(SPISettings(SPI_SPEED, SPI_ORDER, SPI_MODE));
   digitalWrite(_latchPin, LOW);
 
   // Let's talk
   uint8_t data[3] = {
      0x58,
      dataIn >> 8,
      dataIn & 0xFF
   };

   // Transfer the data
   SPI.transfer(&data, 3);

   // Disable communications
   digitalWrite(_latchPin, HIGH);              // Set pin HIGH
   SPI.endTransaction();
}  

void DACX760::writeDACZeroCalibrationRegister(int16_t dataIn)
{
   #ifdef SerialEN
      // Print some data to the serial connection
      Serial.print("Write DAC Zero Calibration Register (0x59): ");
      Serial.println(dataIn, BIN);
   #endif

   // 0x59 -> Write DAC zero calibration register
   SPI.beginTransaction(SPISettings(SPI_SPEED, SPI_ORDER, SPI_MODE));
   digitalWrite(_latchPin, LOW);
 
   // Let's talk
   uint8_t data[3] = {
      0x59,
      dataIn >> 8,
      dataIn & 0xFF
   };

   // Transfer the data
   SPI.transfer(&data, 3);

   // Disable communications
   digitalWrite(_latchPin, HIGH);              // Set pin HIGH
   SPI.endTransaction();
}

void DACX760::watchdogTimerReset()
{
   #ifdef SerialEN
      // Print some data to the serial connection
      Serial.print("Write Watchdog timer reset (0x95)");
   #endif

   // 0x95 -> Watchdog timer reset
   // Start talking
   SPI.beginTransaction(SPISettings(SPI_SPEED, SPI_ORDER, SPI_MODE));
   digitalWrite(_latchPin, LOW);
 
   // Let's talk
   uint8_t data[1] = {
      0x95,
   };

   // Transfer the data
   SPI.transfer(&data, 1);

   // Disable communications
   digitalWrite(_latchPin, HIGH);              // Set pin HIGH
   SPI.endTransaction();
}
