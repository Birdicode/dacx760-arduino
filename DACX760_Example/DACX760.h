#ifndef DACX760_H
#define DACX760_H

/**
 * Include our definitions
 */
#include "Arduino.h"

class DACX760
{
  public:
      enum modes {
         M12bit,                    // 12 bit mode
         M16bit                     // 16 bit mode
      };

      enum outputType {
         VOUT,                      // Voltage Output
         IOUT                       // Current Output
      };

      enum outputLimit {
         I4_20mA     = 0b101,       // 4-20mA
         I0_20mA     = 0b110,       // 0-20mA
         I0_24mA     = 0b111,       // 0-24mA

         U0_5V       = 0b000,       // 0-5V
         U0_10V      = 0b001,       // 0-10V
         UB5V        = 0b010,       // ±5V      (Bipolar)
         UB10V       = 0b011,       // ±10V     (Bipolar)

         DisableI    = 0b000,        // Disable the current output
         DisableV    = 0b1111       // Disable the current output
      };

      /**
       * Init
       */
      DACX760(int latchPin, modes mode);

      /**
       * Does some additional initialization
       */
      void setup();
      
      void enableSingleOutput(outputType outType, outputLimit outLimit, uint16_t value, bool boost);
      void enableDualOutput(outputLimit outLimitVoltage, outputLimit outLimitCurrent, uint16_t valueVoltage, uint16_t valueCurrent, bool boost);

      /**
       * Write Data to the DAC
       * 
       * Min Value: 0
       * Max Value: 65535 / 0xFFFF (16Bit), 4095 / 0xFFF (12bit)
       */
      void writeData(uint16_t data);

      /**
       * 
       * XX XX00 Read status register
       * XX XX01 Read DAC data register
       * XX XX10 Read control register
       * 00 1011 Read configuration register
       * 01 0011 Read DAC gain calibration register
       * 01 0111 Read DAC zero calibration register
       */
      uint8_t readValue(uint16_t registerAddr);
      
      /**
       * DB15        CLRSEL            0                          VOUT clear value select bit.
       *                                                          When bit = 0, VOUT is 0 V in DAC Clear mode or after reset.
       *                                                          When bit = 1, VOUT is midscale in unipolar output and negative-full-scale in bipolar output
       *                                                          in DAC Clear mode or after reset.
       *                                                       
       * DB14        OVR               0                          Setting the bit increases the voltage output range by 10%.              -> See Datasheet Page 37
       * DB13        REXT              0                          External current setting resistor enable.
       * DB12        OUTEN             0                          Output enable.
       *                                                          Bit = 1: Output is determined by RANGE bits.
       *                                                          Bit = 0: Output is disabled. IOUT and VOUT are Hi-Z.
       *    
       *    
       * DB11:DB8    SRCLK[3:0]        0000                       Slew rate clock control. Ignored when bit SREN = 0                      -> See Datasheet Page 35
       * DB7:DB5     SRSTEP[2:0]       000                        Slew rate step size control. Ignored when bit SREN = 0                  -> See Datasheet Page 35
       *    
       * DB4         SREN              0                          Slew Rate Enable.
       *                                                          Bit = 1: Slew rate control is enabled, and the ramp speed of the output change is
       *                                                          determined by SRCLK and SRSTEP.
       *                                                          Bit = 0: Slew rate control is disabled. Bits SRCLK and SRSTEP are ignored. The output
       *                                                          changes to the new level immediately.
       *    
       * DB3         DCEN              0                          Daisy-chain enable.
       * DB2:DB0     RANGE[2:0]        000                        Output range bits.                                                       -> See Datasheet Page 36/37
      */
      void writeControlRegister(uint8_t range, bool DCEN, bool SREN, uint8_t SRStep, uint8_t SRCLK, bool OUTEN, bool REXT, bool OVR, bool CLRSEL);
      
      /**
       * DB15:DB1                      0000h                      Reserved. Writing to these bits does not cause any change.
      
       * DB0         RESET             0                          Software reset bit. Writing 1 to the bit performs a software reset to reset all
       *                                                          registers and the ALARM status to the respective power-on reset default value.
       *                                                          After reset completes the RESET bit clears itself
      */
      void writeResetRegister();
      
      
      /**
       * DB15:DB11                     0h                         Reserved. User must not write any value other than zero to these bits.
       * DB10:DB9       IOUT RANGE     00                         IOUT range. These bits are only used if both voltage and current outputs are
       *                                                          simultaneously enabled through bit 8 (DUAL OUTEN). The voltage output
       *                                                          range is still controlled by bits 2:0 of the Control Register (RANGE bits). The
       *                                                          current range is controlled by these bits and has similar behavior to
       *                                                          RANGE[1:0] when RANGE[2] = 1. However, unlike the RANGE bits, a change
       *                                                          to this field does not make the DAC data register go to its default value.
       * 
       * DB8            DUAL OUTEN     0                          DAC dual output enable. This bit controls if the voltage and current outputs are
       *                                                          enabled simultaneously. Both are enabled when this bit is high. However, both
       *                                                          outputs are controlled by the same DAC data register.
       * 
       * DB7            APD            0                          Alternate power down. On power-up, +VSENSE is connected to the internal
       *                                                          VOUT amplifier inverting terminal. Diodes exist at this node to REFIN and
       *                                                          GND. Setting this bit connects this node to ground through a resistor. When
       *                                                          set, the equivalent resistance seen from +VSENSE to GND is 70 kΩ. This is
       *                                                          useful in applications where the VOUT and IOUT terminals are tied together.
       * 
       * DB6                           0                          Reserved. Do not write any value other than zero to these bits.
      
       * DB5            CALEN          0                          User calibration enable. When user calibration is enabled, the DAC data are
       *                                                          adjusted according to the contents of the gain and zero calibration registers.
       *                                                          See User Calibration.
       * 
       * DB4            HARTEN         0                          Enable interface through HART-IN pin (only valid for IOUT set to 4-mA to 20-
       *                                                          mA range through RANGE bits).
       *                                                          Bit = 1: HART signal is connected through internal resistor and modulates
       *                                                          output current.
       *                                                          Bit = 0: HART interface is disabled.
       * 
       * DB3            CRCEN          0                          Enable frame error checking.
       * DB2            WDEN           0                          Watchdog timer enable.
       * DB1:DB0        WDPD[1:0]      00                         Watchdog timeout period.
      */
      void writeConfigRegister(uint8_t WDPD, bool WDEN, bool HARTEN, bool CALEN, bool APD, bool DUAL_OUTEN, uint8_t range);
      
      /*
       * Voltage and current gain calibration register for user calibration. Format is
       * unsigned straight binary.
       */
      void writeDACGainCalibrationRegister(uint16_t data);

      /**
       *    CODE_OUT = CODE * (User_GAIN+2^15)/2^16 + User_ZERO
       *
       *     - CODE is the decimal equivalent of the code loaded to the DAC data register at address 0x01.
       *     - N is the bits of resolution; 16 for DAC8760 and 12 for DAC7760.
       *     - User_ZERO is the signed 16-bit code in the zero register.
       *     - User_GAIN is the unsigned 16-bit code in the gain register.
       *     - CODE_OUT is the decimal equivalent of the code loaded to the DAC (limited between 0x0000 to 0xFFFF for
       *       DAC8760 and 0x000 to 0xFFF for DAC7760). 
       *
       *     Voltage and current zero calibration register for user calibration. Format is twos
       *     complement.
       */
      void writeDACZeroCalibrationRegister(int16_t data);
      
      /**
       * Reset the watchdog timer
       */
      void watchdogTimerReset();

      private:
        uint8_t _latchPin;            // The latch pin
        modes _mode;                  // 12 or 16bit mode
};

#endif
