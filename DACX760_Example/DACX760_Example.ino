// Include our classes
#include "definitions.h"
#include "DACX760.h"

/**
 * Setup our DAC, latch pin = 10
 *
 * MOSI = 11 (Master out slave in)
 * MISO = 12 (Master in salve out)
 * SCK  = 13 (Clock)
 */
DACX760 dac(10, DACX760::M12bit);


void setup()
{
   #ifdef SerialEN
      // Enable Serial communication with the PC
      Serial.begin(9600);
   #endif

   // Setup our DAC
   dac.setup();

   // Write a command (4-20mA, Output 20mA)
   dac.enableSingleOutput(DACX760::IOUT, DACX760::I4_20mA, 4095, 0);
}


void loop(){}
